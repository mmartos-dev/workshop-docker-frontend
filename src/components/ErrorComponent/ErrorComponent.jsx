import React, { Component } from 'react';
import imgWrong  from'./wrong.png';
import Card from "components/Card/Card.jsx";
import CardHeader from "components/Card/CardHeader.jsx";
import CardBody from "components/Card/CardBody.jsx";
import withStyles from "@material-ui/core/styles/withStyles";
import cardImagesStyles from "assets/jss/material-dashboard-react/cardImagesStyles.jsx";

class ErrorComponent extends Component {
  render() {
    const { classes } = this.props;
    return (
      <Card style={{ width: "25rem", height: "35rem", marginLeft: "auto", marginRight: "auto" }}>
        <CardHeader style={{ backgroundColor: "#e0e0e0", paddingBottom: "0" }}>
          <img className={classes.cardImgTop} src={imgWrong}  style={{ width: "100%", display: "block" }} alt="Oops, something went wrong"/>
        </CardHeader>
        <CardBody>
          <h3>Oops, something went wrong</h3>
          <h4>{this.props.message}</h4>
        </CardBody>
      </Card>
    );
  }
}

export default withStyles(cardImagesStyles)(ErrorComponent);
