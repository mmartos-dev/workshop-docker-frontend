import React, { Component } from 'react';
import GridItem from "components/Grid/GridItem.jsx";
import GridContainer from "components/Grid/GridContainer.jsx";
import Table from "components/Table/Table.jsx";
import Card from "components/Card/Card.jsx";
import CardHeader from "components/Card/CardHeader.jsx";
import CardBody from "components/Card/CardBody.jsx";
import withStyles from "@material-ui/core/styles/withStyles";
import cardImagesStyles from "assets/jss/material-dashboard-react/cardImagesStyles.jsx";
import { withRouter, Redirect } from 'react-router'

class Tasks extends Component {
  constructor(props) {
    super(props);
  }

  handleTaskClick = (event) => {
    const task = this.props.tasks[event.target.id];
    console.log("Task clicked" + task.id);
    console.log("props -> " + JSON.stringify(this.props));
    this.props.history.push('/task', {data: task});
  }

  render() {
    const { classes } = this.props;
    const tableData = [];
    for (var idx = 0; idx < this.props.tasks.length; idx++) {
      const task = this.props.tasks[idx];
      tableData.push([ idx + 1, task.creationDate, task.status, task.name ]);
    }
    return (
      <GridContainer>
        <GridItem xs={12} sm={12} md={12}>
          <Card>
            <CardHeader color="warning">
              <h4 className={classes.cardTitleWhite} style={{ margin: 0 }}>TODO Tasks</h4>
              <p style={{ margin: 0 }}>Here you have the complete list of tasks with it's status</p>
            </CardHeader>
            <CardBody>
              <Table 
                  tableHeaderColor="warning"
                  tableHead={["ID", "Creation Date", "Status", "Name"]}
                  tableData={tableData}
                  tableCellClick={this.handleTaskClick}
                  />
            </CardBody>
          </Card>
        </GridItem>
      </GridContainer>
    );
  }
}

export default withRouter(withStyles(cardImagesStyles)(Tasks));
