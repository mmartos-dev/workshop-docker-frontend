import React from "react";
import PropTypes from "prop-types";
// @material-ui/core
import withStyles from "@material-ui/core/styles/withStyles";
import Icon from "@material-ui/core/Icon";
// @material-ui/icons
import Store from "@material-ui/icons/Store";
import Warning from "@material-ui/icons/Warning";
import DateRange from "@material-ui/icons/DateRange";
import LocalOffer from "@material-ui/icons/LocalOffer";
import Update from "@material-ui/icons/Update";
import ArrowUpward from "@material-ui/icons/ArrowUpward";
import AccessTime from "@material-ui/icons/AccessTime";
import Accessibility from "@material-ui/icons/Accessibility";
import BugReport from "@material-ui/icons/BugReport";
import Code from "@material-ui/icons/Code";
import Cloud from "@material-ui/icons/Cloud";
import FormControl from "@material-ui/core/FormControl";
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import CircularProgress from '@material-ui/core/CircularProgress';
// core components
import CustomInput from "components/CustomInput/CustomInput.jsx";
import GridItem from "components/Grid/GridItem.jsx";
import GridContainer from "components/Grid/GridContainer.jsx";
import Button from "components/CustomButtons/Button.jsx";
import Table from "components/Table/Table.jsx";
import Tasks from "components/Tasks/Tasks.jsx";
import CustomTabs from "components/CustomTabs/CustomTabs.jsx";
import Danger from "components/Typography/Danger.jsx";
import Card from "components/Card/Card.jsx";
import CardHeader from "components/Card/CardHeader.jsx";
import CardIcon from "components/Card/CardIcon.jsx";
import CardBody from "components/Card/CardBody.jsx";
import CardFooter from "components/Card/CardFooter.jsx";
import ErrorComponent from "components/ErrorComponent/ErrorComponent.jsx"

import { bugs, website, server } from "variables/general.jsx";

import {
  dailySalesChart,
  emailsSubscriptionChart,
  completedTasksChart
} from "variables/charts.jsx";

import dashboardStyle from "assets/jss/material-dashboard-react/views/dashboardStyle.jsx";
import { TextField } from "@material-ui/core";

class TaskDetail extends React.Component {
  constructor(props) {
    super(props);
    this.state = this.props.location.state;
  }

  handleChange = key => event => {
    this.setState({data: Object.assign({}, this.state.data, { [key]: event.target.value })});
  };

  handleUpdate = () => {
    const url = `${process.env.REACT_APP_SERVER_URL}/tasks/${this.state.data.id}`;
    this.setState({data: this.state.data, updating: "true"});
    const newData = Object.keys(this.state.data).map((key) => {
      return encodeURIComponent(key) + '=' + encodeURIComponent(this.state.data[key]);
    }).join('&');
    fetch(url, {
        method: 'PUT',
        mode: 'cors',
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        },
        body: newData
      })
      .then(res => res.json())
      .then(
        result => this.setState({data: this.state.data, updating: "false", updatethisd: "true"}),
        error => this.setState({data: this.state.data, updating: "false", updated: "false"})
      );
  }

  render() {
    const { classes } = this.props;
    const { data, updating } = this.state;
    return (
      <div>
        <GridContainer>
          <GridItem xs={12} sm={12} md={12}>
            <Card>
              <CardHeader color="primary">
                <h4 className={classes.cardTitleWhite}>Edit Task</h4>
                <p className={classes.cardCategoryWhite}>Update the bellow data</p>
              </CardHeader>
              <CardBody>
                <GridContainer>
                  <GridItem xs={12} sm={12} md={2}>
                    <CustomInput
                      labelText="Id (disabled)"
                      id="id-disabled"
                      formControlProps={{
                        fullWidth: true
                      }}
                      inputProps={{
                        disabled: true,
                        value: data.id
                      }}
                    />
                  </GridItem>
                </GridContainer>
                <GridContainer>
                  <GridItem xs={9} sm={9} md={9}>
                    <CustomInput
                      labelText="Name"
                      id="Name"
                      formControlProps={{
                        fullWidth: true
                      }}
                      inputProps={{
                        type: "text",
                        value: data.name,
                        onChange: this.handleChange('name')
                      }}                      
                    />
                  </GridItem>
                  <GridItem xs={2} sm={2} md={2}>
                    <FormControl className={classes.formControl + classes.labelRoot} fullWidth="true" style={{ marginTop: 27 }}>
                      <InputLabel htmlFor="age-simple">Status</InputLabel>
                      <Select
                        value={data.status}
                        onChange={this.handleChange('status')}
                        inputProps={{
                          name: 'age',
                          id: 'age-simple'                          
                        }}
                      >
                        <MenuItem value="pending">Pending</MenuItem>
                        <MenuItem value="ongoing">Ongoing</MenuItem>
                        <MenuItem value="completed">Completed</MenuItem>
                      </Select>
                    </FormControl>
                  </GridItem>
                </GridContainer>
                <GridContainer justify="flex-end">
                <GridItem xs={2} sm={2} md={2}>
                    <CardFooter>
                      <Button color="primary" onClick={this.handleUpdate}>Update Task</Button>
                    </CardFooter>
                  </GridItem>
                  <GridItem xs={2} sm={2} md={2}>
                    <CardFooter>
                      <Button color="secondary">Delete Task</Button>
                    </CardFooter>
                  </GridItem>
                </GridContainer>
              </CardBody>
            </Card>
          </GridItem>
        </GridContainer>
      </div>
    );
  }
}

TaskDetail.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(dashboardStyle)(TaskDetail);
