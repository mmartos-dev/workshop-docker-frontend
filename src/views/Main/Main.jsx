import React from "react";
import PropTypes from "prop-types";
// @material-ui/core
import withStyles from "@material-ui/core/styles/withStyles";
import Icon from "@material-ui/core/Icon";
// @material-ui/icons
import Store from "@material-ui/icons/Store";
import Warning from "@material-ui/icons/Warning";
import DateRange from "@material-ui/icons/DateRange";
import LocalOffer from "@material-ui/icons/LocalOffer";
import Update from "@material-ui/icons/Update";
import ArrowUpward from "@material-ui/icons/ArrowUpward";
import AccessTime from "@material-ui/icons/AccessTime";
import Accessibility from "@material-ui/icons/Accessibility";
import BugReport from "@material-ui/icons/BugReport";
import Code from "@material-ui/icons/Code";
import Cloud from "@material-ui/icons/Cloud";
// core components
import GridItem from "components/Grid/GridItem.jsx";
import GridContainer from "components/Grid/GridContainer.jsx";
import Table from "components/Table/Table.jsx";
import Tasks from "components/Tasks/Tasks.jsx";
import CustomTabs from "components/CustomTabs/CustomTabs.jsx";
import Danger from "components/Typography/Danger.jsx";
import Card from "components/Card/Card.jsx";
import CardHeader from "components/Card/CardHeader.jsx";
import CardIcon from "components/Card/CardIcon.jsx";
import CardBody from "components/Card/CardBody.jsx";
import CardFooter from "components/Card/CardFooter.jsx";
import ErrorComponent from "components/ErrorComponent/ErrorComponent.jsx"

import { bugs, website, server } from "variables/general.jsx";

import {
  dailySalesChart,
  emailsSubscriptionChart,
  completedTasksChart
} from "variables/charts.jsx";

import dashboardStyle from "assets/jss/material-dashboard-react/views/dashboardStyle.jsx";

class Main extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      isLoaded: false,
      items: []
    };
  }

  componentDidMount() {
    const url = `${process.env.REACT_APP_SERVER_URL}/tasks`;
    console.log(url);
    fetch(url)
      .then(res => res.json())
      .then(
        result => this.setState({isLoaded: true, items: result}),
        error => this.setState({isLoaded: true, error})
      );
  }

  render() {
    const { classes } = this.props;
    const { error, isLoaded, items } = this.state;
    if (error) {
      return <ErrorComponent message={error.message} />;
    } else if (!isLoaded) {
      return (
        <div>
          <GridContainer>
            <GridItem xs={12} sm={6} md={3}>
              <Card>
                <CardFooter stats>
                    <div>Loading...</div>
                </CardFooter>
              </Card>
            </GridItem>
          </GridContainer>
        </div>
      );
    } else {
      return <Tasks tasks={items} />;
    }
  }
}

Main.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(dashboardStyle)(Main);
